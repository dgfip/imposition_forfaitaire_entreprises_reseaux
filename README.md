Le montant de l'impôt dû sur un avis d'impôt de cotisation foncière des
entreprises (CFE) correspond à la somme des cotisations de CFE, des
taxes consulaires (taxe pour frais de chambres de commerce et
d'industrie, taxe pour frais de chambres des métiers et de l'artisanat)
ainsi que des différentes composantes de l'imposition forfaitaire sur
les entreprises de réseaux.

Voici, étape par étape, le mode opératoire permettant de calculer le
« Montant de votre impôt » figurant sur votre avis de CFE.

# I. CALCUL DE LA COTISATION DE CFE

## 1. La base brute (case n° 1)
----------------------------

L'avis de CFE regroupe pour un redevable l'ensemble des locaux dont il
est exploitant à une adresse d'imposition dans une commune, quelle que
soit la nature des locaux (locaux d'habitation, locaux professionnels ou
locaux industriels).

Depuis 2017, la cotisation des locaux professionnels est « révisée »,
c'est-à-dire calculée sur une nouvelle base établie avec une méthode
nouvelle de calcul de la valeur locative. Le montant de la valeur
locative des locaux révisés inclus dans la valeur locative brute est
mentionné dans la case n° 2.

Pour plus de précisions sur la révision des valeurs locatives des locaux
professionnels, il convient de se reporter à la rubrique dédiée sur le
site impots.gouv.fr :

Accueil \> Professionnel \> Gérer mon entreprise / association \> Je
suis propriétaire ou je suis occupant d'un local professionnel \> [Les
grands principes de la révision des valeurs locatives des locaux
professionnels](https://www.impots.gouv.fr/portail/professionnel/les-grands-principes-de-la-revision-des-valeurs-locatives-des-locaux-professionnels).

Dans le cadre de la réforme des impôts de production, les paramètres
concourant à la détermination de la valeur locative (VL) des
établissements industriels évalués selon la méthode comptable ont été
modifiés à compter de 2021 de manière à réduire de moitié la VL de ces
établissements et donc l'assiette imposable. La présence d'un
établissement industriel évalué selon la méthode comptable dans la base
d'imposition est mentionnée dans la case n° 3 par le mot « OUI ».

La valeur locative brute mentionnée dans la case n° 1 tient également
compte de l'abattement prorata temporis en faveur des activités
saisonnières mentionnées à l'article [1478](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000025092116?etatTexte=VIGUEUR&etatTexte=VIGUEUR_DIFF) V du code général des impôts (CGI). Le nombre de semaines déclaré est mentionné dans la case n° 4.

## 2. La base réduite (case n° 8)
------------------------------

La base brute constituée de la valeur locative brute peut faire l'objet
de réductions de bases de 50 % en cas de création d'établissement (case
n° 5) ou de 75 %, 50 % ou 25 % en faveur des artisans et bateliers qui
emploient respectivement 1, 2 ou 3 salariés, ou de 50 % en faveur de
certaines coopératives agricoles et de leurs unions (case n° 6).

Le total des réductions de base est mentionné dans la case n° 7.

(case n° 7) = (case n° 5) + (case n° 6)

La base après réductions est mentionnée dans la case n° 8.

(case n° 8) = (case n° 1) - (case n° 7)

## 3. La base nette (ligne n° 15)
------------------------------

Généralement, la base nette d'imposition est égale à la base réduite
(après abattement de 25 % en Corse).

### 3.1. La base minimum (cases n° 9 à 12)

Lorsque la base réduite est inférieure à la base minimum, cette dernière
est retenue dans le cas d'une imposition établie au lieu du principal
établissement et la mention « oui » apparaît dans la case n° 9. Le
montant de la base minimum, mentionné dans la case n° 12, est fixé par
le conseil municipal ou par l'établissement public de coopération
intercommunale. Il peut être modulé par les collectivités locales en
fonction de la situation du chiffre d'affaires ou des recettes réalisés
par l'entreprise au cours de la période de référence par rapport aux
seuils de 10 000, 32 600, 100 000, 250 000 et 500 000 €. Le montant de
chiffre d'affaires ou de recettes pris en compte pour la détermination
de la base minimum est mentionné dans la case n° 10.

Les délibérations des collectivités locales visant à accorder une
réduction de base minimum limitée à 50 % aux redevables à temps partiel
ou exerçant moins de neuf mois de l'année continuent de s'appliquer tant
qu'elles ne sont pas rapportées. La case n° 11 est renseignée de la
mention « oui » en cas d'application d'une telle délibération.

### 3.2. Les bases exonérées (lignes 13 et 14)

S'il y a lieu, la base nette d'imposition est diminuée pour tenir compte
des exonérations ou abattements prévus.

Ces exonérations étant variables selon la collectivité locale, les bases
exonérées inscrites sur les lignes n° 13 et 14 et les bases nettes
d'imposition inscrites sur la ligne n° 15 peuvent différer selon la
collectivité qui a délibéré en faveur de l'exonération. Pour
information, la ligne n° 14 indique le montant des bases exonérées qui
sert à déterminer la cotisation exonérée de la case n° 22 qui permet de
limiter le paiement des acomptes et du solde de la cotisation sur la
valeur ajoutée des entreprises (CVAE).

(ligne n° 15) = sup (case n° 8 ; case n° 12) - (ligne n° 13)

En Corse : ce résultat est multiplié par 0,75 pour tenir compte de
l'abattement de 25 %.

## 4. Les taux d'imposition (ligne n° 17)
--------------------------------------

Chaque année, les taux d'imposition sont votés par chaque collectivité
territoriale (commune et intercommunalité). La multiplication de chaque
taux de la ligne n° 17 par le montant de la base nette de la ligne n° 15
permet d'obtenir le montant de la CFE par collectivité et par taxe
annexe (taxes spéciales d'équipement ou taxe GEMAPI[^1]).

## 5. Cotisation avant lissage (ligne n° 18) = base nette (ligne n° 15) x taux d'imposition (ligne n° 17)
------------------------------------------------------------------------------------------------------

La cotisation est calculée pour chaque collectivité et chaque taxe
annexe.

En présence d'un local professionnel évalué selon le mode révisé, cette
cotisation est calculée avant application du dispositif de lissage sur
10 ans.

## 6. Cotisation lissée (ligne n° 19) 
-----------------------------------

En présence d'au moins un local professionnel imposé en 2017 et n'ayant
pas subi de modification de plus de 10 % de sa surface depuis 2017, un
dispositif de lissage s'applique à la cotisation de CFE pour chaque
collectivité et taxe annexe. Ce dispositif de lissage consiste en un
étalement sur 10 ans, de 2017 à 2026, de la hausse ou de la baisse de la
cotisation révisée par rapport à la cotisation qui aurait été calculée
selon l'ancien système de calcul.

En l'absence de local révisé, la ligne « Cotisation lissée » est
identique à la ligne « Cotisation avant lissage ».

Le montant global du lissage appliqué à la cotisation est indiqué en fin
d'avis en lignes 190 et 191.

## 7. Total des cotisations lissées (case n° 23) 
----------------------------------------------

La case « Total des cotisations lissées » restitue le montant total de
CFE due. Ce montant est la somme des cotisations lissées par
collectivité et par taxe annexe déclinées en ligne 19.

## 8. Les frais de gestion (case n° 24) \[article [1641](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041465113?etatTexte=VIGUEUR&etatTexte=VIGUEUR_DIFF) du CGI\] 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de la CFE. Ces frais s'élèvent à :

- 3 % de la cotisation de taxe GEMAPI ;

- 8 % de la cotisation syndicale ;

- 9 % de la cotisation de TSE.

Les régions, quant à elles, perçoivent 3 % du montant des cotisations de
CFE revenant aux communes et aux EPCI pour l'exercice de leurs
compétences en matière de formation professionnelle et d'apprentissage.

## 9. Total de CFE (ligne 25) = total des cotisations lissées (case 23) + frais de gestion (case 24)
-------------------------------------------------------------------------------------------------

Le total de CFE est obtenu en additionnant le total des cotisations
lissées restitué en case 23 et les frais de gestion mentionnés en case
24.

Ce montant est compris dans le total mentionné en ligne 192.

# II. CALCUL DE LA COTISATION DE TCCI

Les redevables de la CFE sont en principe assujettis à la taxe pour
frais de chambres de commerce et d'industrie (TCCI), sauf ceux
expressément exonérés par l'article [160](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042908122?etatTexte=VIGUEUR&etatTexte=VIGUEUR_DIFF)[0](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042908122?etatTexte=VIGUEUR&etatTexte=VIGUEUR_DIFF) du CGI. Les micro-entrepreneurs bénéficiant d'un régime dérogatoire auprès des organismes de la sécurité sociale ne sont pas concernés par cette rubrique.

## 10. Base d'imposition (case 26) 
--------------------------------

La base d'imposition à la TCCI est en principe identique à celle retenue
en matière de CFE en ligne 15 hormis en présence de base spécifique
imposable à la TCCI ou en présence d'exonérations indiquées en ligne 13
pour la CFE et en case 29 pour la TCCI. Cette base d'imposition est
réduite de moitié pour les artisans régulièrement inscrits au répertoire
des métiers et qui restent portés sur la liste électorale de la chambre
de commerce et d'industrie de leur circonscription (redevables dont le
cadre TCMA de l'avis d'imposition est rempli).

## 11. Taux d'imposition (case 27)
-------------------------------

Le taux de cette taxe sera égal à 0,89 % à compter de 2023. En 2021, le
taux applicable à chaque établissement est égal à la somme de quatre
dix-neuvièmes du taux de 0,89 % et de quinze dix-neuvièmes du taux voté
en 2019 par la CCI de région dans le ressort de laquelle il se trouve.
En 2022, la taux applicable à chaque établissement est égal à la somme
de quatorze dix-neuvièmes du taux de 0,89 % et de cinq dix-neuvièmes du taux voté en 2019 par la CCI de région dans le ressort de laquelle il se
trouve.

## 12. Cotisation avant lissage (case 28) = base d'imposition (case 26) x taux (case 27)
-------------------------------------------------------------------------------------

En présence d'un local professionnel révisé, cette cotisation est
calculée avant application du dispositif de lissage sur 10 ans.

## 13. Cotisation lissée (case 31)
-------------------------------

En présence d'au moins un local professionnel imposé en 2017 et n'ayant
pas subi de modification de plus de 10 % de sa surface depuis 2017, un
dispositif de lissage s'applique à la cotisation de TCCI. Ce dispositif
de lissage consiste en un étalement sur 10 ans, de 2017 à 2026, de la
hausse ou de la baisse de la cotisation révisée par rapport à la
cotisation qui aurait été calculée selon l'ancien système de calcul.

En l'absence de local évalué selon le mode révisé, la ligne « Cotisation
lissée » est identique à la ligne « Cotisation avant lissage ».

Le montant global du lissage appliqué à la cotisation est indiqué en fin
d'avis en lignes 190 et 191.

## 14. Frais de gestion (case n° 34) \[article [1641](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041465113?etatTexte=VIGUEUR&etatTexte=VIGUEUR_DIFF) du CGI\]
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de la TCCI. Ces frais s'élèvent à 9 % de la cotisation
de TCCI.

## 15. Cotisation totale de TCCI (case 35) = cotisation lissée (case 31) + frais de gestion (case 34) 
---------------------------------------------------------------------------------------------------

Le total de TCCI est obtenu en sommant le total des cotisations lissées
restitué en case 31 et les frais de gestion mentionnés en case 34.

Ce montant est compris dans le total mentionné en ligne 192.

# III. CALCUL DE LA COTISATION DE TCMA
La taxe pour frais de chambres des métiers et de l'artisanat (TCMA) est
acquittée par les entreprises tenues de s'inscrire au répertoire des
métiers et celles qui s'y immatriculent volontairement. Les
micro-entrepreneurs bénéficiant d'un régime dérogatoire auprès des
organismes de la sécurité sociale ne sont pas concernés par cette
rubrique.

La TCMA comprend un droit fixe par ressortissant et un droit additionnel
à la CFE.

## 16. Droit fixe (case 44)
------------------------

Le droit fixe par ressortissant est dû par tous les redevables de la
taxe (sauf les établissements principaux ou uniques imposés sur la base
minimum dont le montant du chiffre d'affaires réalisé au cours de la
période de référence est inférieur ou égal à 5 000 €). Son montant
déterminé par CMA France est restitué dans la case 44.

## 17. Droit additionnel à la CFE
------------------------------

### 17.1. Base d'imposition (case 36)

La base d'imposition à la TCMA est en principe identique à celle retenue
en matière de CFE en ligne 15 hormis en présence de base spécifique
imposable à la TCMA ou en présence d'exonérations indiquées en ligne 13 pour la CFE et en case 37 pour la TCMA.

### 17.2. Taux d'imposition (case 38)

Le taux du droit additionnel à la CFE est obtenu en divisant son produit
par la somme des bases de CFE des artisans imposables. Le produit total
du droit additionnel à la CFE est arrêté par CMA France entre 60 % et
90 % du produit du droit fixe.

### 17.3. Cotisation avant lissage (case 40) = base d'imposition (case 36) x taux (case 38)

En présence d'un local professionnel évalué selon le mode révisé, cette
cotisation est calculée avant application du dispositif de lissage sur
10 ans.

### 17.4. Cotisation lissée (case 42)

En présence d'au moins un local professionnel imposé en 2017 et n'ayant
pas subi de modification de plus de 10 % de sa surface depuis 2017, un
dispositif de lissage s'applique à la cotisation de TCMA. Ce dispositif
de lissage consiste en un étalement sur 10 ans, de 2017 à 2026, de la
hausse ou de la baisse de la cotisation révisée par rapport à la
cotisation qui aurait été calculée selon l'ancien système de calcul.

En l'absence de local évalué selon le mode révisé, la ligne « Cotisation
lissée » est identique à la ligne « Cotisation avant lissage ».

Le montant global du lissage appliqué à la cotisation est indiqué en fin
d'avis en lignes 190 et 191.

## 18. Total de TCMA (case 45) = total droits fixes (case 44) + cotisation lissée (case 42)
----------------------------------------------------------------------------------------

Le total de TCMA indiqué en case 45 est obtenu en additionnant le
montant du droit fixe mentionné en case 44 et le montant de cotisation
lissée indiqué en case 42.

## 19. Frais de gestion (case 46) \[article [1641](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041465113?etatTexte=VIGUEUR&etatTexte=VIGUEUR_DIFF) du CGI\]
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de la TCMA. Ces frais s'élèvent à 9 % de la cotisation
de TCMA hors Alsace et Moselle où ces frais s'élèvent à 8 % de la
cotisation de TCMA.

## 20. Cotisation totale de TCMA (case 47) = Total de TCMA (case 45) + frais de gestion (case 46)
----------------------------------------------------------------------------------------------

La cotisation totale de TCMA est obtenue en additionnant le total de
TCMA restitué en case 45 et les frais de gestion mentionnés en case 46.

Ce montant est compris dans le total mentionné en ligne 192.

# IV. CALCUL DE LA COTISATION D'IFER

L'imposition forfaitaire sur les entreprises de réseaux (IFER) est
constituée de dix composantes correspondant chacune à une catégorie de
biens imposables. Les montants et tarifs de chacune des composantes de
l'IFER sont revalorisés chaque année comme le taux prévisionnel, associé
au projet de loi de finances de l'année, d'évolution des prix à la
consommation des ménages, hors tabac, pour la même année.

Le montant de l'IFER est porté sur l'avis de CFE.

## 21. Imposition sur les éoliennes et les hydroliennes
----------------------------------------------------

### 21.1. Cotisation de l'IFER sur les éoliennes (case 49) = puissance électrique en kW (case 48) x tarif

### 21.2. Cotisation de l'IFER sur les hydroliennes (case 54) = puissance électrique en kW (case 53) x tarif

### 21.3. Total des cotisations dues au titre des installations de production d'électricité (case 58) = cotisation de l'IFER sur les éoliennes (case 49) + cotisation de l'IFER sur les hydroliennes (case 54)

### 21.4. Frais de gestion (case 59)

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 21.5. Total de l'imposition sur les éoliennes et les hydroliennes (case 60) = total des cotisations dues au titre des installations de production d'électricité (case 58) + frais de gestion (case 59)

Ce montant est compris dans le total mentionné en ligne 192.

## 22. Imposition sur les installations de production d'électricité d'origine nucléaire ou thermique à flamme
----------------------------------------------------------------------------------------------------------

### 22.1. Cotisation de l'IFER (case 62) = puissance électrique en MW (case 61) x tarif

### 22.2. Frais de gestion (case 67)

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 22.3. Total de l'imposition sur les centrales nucléaires ou thermiques à flamme (case 68) = cotisation de l'IFER (case 62) + frais de gestion (case 67) 

Ce montant est compris dans le total mentionné en ligne 192.

## 23. Imposition sur les centrales de production d'électricité d'origine photovoltaïque ou hydraulique 
-----------------------------------------------------------------------------------------------------

### 23.1. Cotisation de l'IFER sur les centrales photovoltaïques (case 70) = puissance électrique en kW (case 69) x tarif

### 23.2. Cotisation de l'IFER sur les centrales hydrauliques (case 76) = puissance électrique en kW (case 74) x tarif

### 23.3. Total des cotisations dues au titre des installations de production d'électricité (case 80) = cotisation de l'IFER sur les centrales photovoltaïques (case 70) + cotisation de l'IFER sur les centrales hydrauliques (case 76)

### 23.4. Frais de gestion (case 81)

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 23.5. Total de l'imposition sur les centrales photovoltaïques et hydrauliques (case 82) = total des cotisations dues au titre des installations de production d'électricité (case 80) + frais de gestion (case 81)

Ce montant est compris dans le total mentionné en ligne 192.

## 24. Imposition sur les transformateurs électriques

### 24.1. Cotisation sur les transformateurs dont la tension en amont est supérieure à 50 kV et inférieure ou égale à 130 kV (case 84) = nombre de ce type de transformateurs (case 83) x tarif

### 24.2. Cotisation sur les transformateurs dont la tension en amont est supérieure à 130 kV et inférieure ou égale à 350 kV (case 86) = nombre de ce type de transformateurs (case 85) x tarif

### 24.3. Cotisation sur les transformateurs dont la tension en amont est supérieure à 350 kV (case 88) = nombre de ce type de transformateurs (case 87) x tarif

### 24.4. Total des cotisations dues au titre des transformateurs électriques (case 89) = somme des cotisations pour les trois types de transformateurs (case 84) + (case 86) + (case 88)

### 24.5. Frais de gestion (case 92)

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 24.6. Total de l'IFER due sur les transformateurs électriques (case 93) = total des cotisations dues au titre des transformateurs (case 89) + frais de gestion (case 92)

Ce montant est compris dans le total mentionné en ligne 192.

## 25. Imposition sur les stations radioélectriques
------------------------------------------------

L'IFER comporte plusieurs tarifs en fonction de la nature des stations
concernées. Le tarif réduit concerne les petites stations « small
cell ». Durant les trois premières années d'imposition, les stations
relevant du tarif de droit commun et du tarif « zones blanches » sont
imposées au quart de ce tarif. Le montant de l'IFER due à raison d'une
station peut être réparti entre plusieurs redevables lorsque plusieurs
personnes disposent de la même station pour les besoins de leur activité
professionnelle.

### 25.1. Cotisation due pour les stations au tarif de droit commun exploitées par « i » redevables (case 95 « i ») = nombre de stations (case 94 « i ») x tarif / « i »

### 25.2. Cotisation due pour les stations au tarif réduit de droit commun exploitées par « j » redevables (case 97 « j ») = nombre de stations (case 96 « j ») x tarif / « j »

### 25.3. Cotisation due pour les stations au quart de tarif de droit commun exploitées par « k » redevables (case 99 « k ») = nombre de stations (case 98 « k ») x tarif / « k »

### 25.4. Cotisation due pour les stations au quart de tarif réduit de droit commun exploitées par « l » redevables (case 101 « l ») = nombre de stations (case 100 « l ») x tarif / « l »

### 25.5. Cotisation due pour les stations au tarif « zones blanches » exploitées par « m » redevables (case 103 « m ») = nombre de stations (case 102 « m ») x tarif / « m »

### 25.6. Cotisation due pour les stations au tarif réduit « zones blanches » exploitées par « n » redevables (case 105 « n ») = nombre de stations (case 104 « n ») x tarif / « n »

### 25.7. Cotisation due pour les stations au quart de tarif « zones blanches » exploitées par « o » redevables (case 107 « o ») = nombre de stations (case 106 « o ») x tarif / « o »

### 25.8. Cotisation due pour les stations au quart de tarif réduit « zones blanches » exploitées par « p » redevables (case 109 « p ») = nombre de stations (case 108 « p ») x tarif / « p »

### 25.9. Cotisation due pour les stations au tarif « loi n° 86-1067 »exploitées par « q » redevables (case 111 « q ») = nombre de stations (case 110 « q ») x tarif / « q »

### 25.10. Total des cotisations dues sur les stations radioélectriques (case 112) = somme des cotisations précédemment déterminées pour chaque type de stations (case 95 « i ») + (case 97 « j ») + (case 99 « k ») + (case 101 « l ») + (case 103 « n ») + (case 105 « n ») + (case (107 « o ») + (case (109 « p ») + case (111 « q »)

### 25.11. Frais de gestion (case 116)

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 25.12. Total de l'IFER sur les stations radioélectriques (ligne 117) = (case 112) + (case 116)

Ce montant est compris dans le total mentionné en ligne 192.

## 26. Imposition sur les installations gazières et sur les canalisations de transport de gaz naturel, d'autres hydrocarbures et de produits chimiques

---------------------------------------------------------------------------------------------------------------------------------------------------

Chaque type d'installation et de canalisation est imposé selon le tarif
correspondant appliqué au nombre de kilomètres de canalisation ou au
prorata communal d'imposition pour les installations de gaz naturel
liquéfié, les sites de stockage souterrains de gaz naturel et les
stations de compression du réseau de transport de gaz naturel. Pour les
installations de gaz naturel liquéfié et les stations de compression, le
prorata communal d'imposition correspond au pourcentage de valeur
locative foncière imposée à la CFE sur la commune par rapport à la somme
des valeurs locatives foncières de l'installation ou de la station
imposée à la CFE sur l'ensemble des communes où s'étend l'établissement.
Pour les sites de stockage, le prorata communal d'imposition correspond
à la somme des pourcentages de surface d'aire de stockage de chaque site
située sous la commune par rapport à la surface totale sous l'ensemble
des communes couverte par chaque site.

### 26.1. Cotisation sur les installations de gaz naturel liquéfié (case 121) = prorata communal d'imposition (case 119) x tarif (case 120) 

Le tarif est déterminé par la situation de la capacité de stockage par
rapport à 100 000 m3.

### 26.2. Cotisation sur les sites de stockage souterrains de gaz naturel (case 126) = prorata communal d'imposition (case 125) x tarif 

### 26.3. Cotisation sur les canalisations de transport de gaz naturel (case 131) = nombre de km (case 130) x tarif

### 26.4. Cotisation sur les stations de compression (case 137) = prorata communal d'imposition (case 136) x tarif

### 26.5. Cotisation sur les canalisations de transport d'autres hydrocarbures (case 141) = nombre de km (case 140) x tarif

### 26.6. Cotisation sur les canalisations de transport de produits chimiques (case 146) = nombre de km (case 145) x tarif

### 26.7. Total des cotisations sur les installations gazières et canalisations de transport (case 150) = somme des cotisations précédemment déterminées sur chaque type d'installation et de canalisation (case 126) + (case 131) + (case 137) + (case 141) + (case 146)

### 26.8. Frais de gestion (case 151) 

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 26.9. Total de l'IFER sur les installations gazières et canalisations de transport (ligne 152) = (case 150) + (case 151) 

Ce montant est compris dans le total mentionné en ligne 192.

## 27. Imposition sur les installations de production d'électricité d'origine géothermique
---------------------------------------------------------------------------------------

### 27.1. Cotisation (case 154) = puissance électrique installée en kW (case 153) x tarif

### 27.2. Frais de gestion (case 157)

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 27.3. Total de l'IFER sur les installations de production d'électricité d'origine géothermique (ligne 158) = cotisation (case 154) + frais de gestion (case 157)

Ce montant est compris dans le total mentionné en ligne 192.

## 28. Imposition sur le matériel ferroviaire roulant utilisé sur le réseau ferré national (RFN) pour les opérations de transport de voyageurs
-------------------------------------------------------------------------------------------------------------------------------------------

Le tarif est fixé en fonction de la nature et de l'utilisation du
matériel roulant.

### 28.1. Cotisation sur les automoteurs à moteur thermique (ligne 159) = nombre x tarif

### 28.2. Cotisation sur les locomotives diesel (ligne 160) = nombre x tarif

### 28.3. Cotisation sur les automotrices à moteur électrique (ligne 161) = nombre x tarif

### 28.4. Cotisation sur les locomotives électriques (ligne 162) = nombre x tarif

### 28.5. Cotisation sur les motrices de matériel à grande vitesse (ligne 163) = nombre x tarif

### 28.6. Cotisation sur les automotrices tram-train (ligne 164) = nombre x tarif

### 28.7. Cotisation sur les remorques pour le transport de voyageurs (ligne 165) = nombre x tarif

### 28.8. Cotisation sur les remorques pour le transport de voyageurs à grande vitesse (ligne 166) = nombre x tarif

### 28.9. Cotisation sur les remorques tram-train (ligne 167) = nombre x tarif

### 28.10. Total des cotisations brutes sur le matériel roulant (ligne 168) = somme des cotisations précédemment déterminées sur chaque type de matériel (ligne 159) + (ligne 160) + (ligne 161) + (ligne 162) + (ligne 163) + (ligne 164) + (ligne 165) + (ligne 166) + (ligne 167)

### 28.11. Coefficient progressif de taxation (case 170)

L\'IFER portant sur le matériel roulant utilisé pour des opérations de
transport de voyageurs est soumise à un dispositif d\'imposition
progressive.

Il s\'agit d\'un système avec deux seuils :
- un seuil bas au-dessous duquel l\'imposition n\'est pas due ;
- et un seuil haut au-dessus duquel l\'imposition est complète.

Entre ces deux bornes, l\'imposition est progressive.

Pour les entreprises de transport ferroviaire qui ont parcouru, l\'année précédant celle de l\'imposition, sur le RFN pour des opérations de transport de voyageurs :

- moins de 300 000 kilomètres, l\'imposition forfaitaire n\'est pas due ;
- entre 300 000 et 1 700 000 kilomètres, l\'imposition est multipliée par un coefficient égal à : (nombre de kilomètres parcourus sur le RFN l\'année précédant celle de l\'imposition - 300  000) / 1 400 000 ;
- plus de 1 700 000 kilomètres, l\'imposition s\'applique pleinement.

### 28.12. Total des cotisations sur le matériel roulant (case 171) = total des cotisations brutes (ligne 168) x coefficient (case 170) 

### 28.13. Frais de gestion (case 172)

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 28.14. Total de l'IFER sur le matériel roulant (ligne 173) = total des cotisations (case 171) + frais de gestion (case 172)

Ce montant est compris dans le total mentionné en ligne 192.

## 29. Imposition sur certains matériels roulants utilisés sur les lignes de transport en commun de voyageurs en Ile-de-France
---------------------------------------------------------------------------------------------------------------------------

Le tarif est fixé en fonction de la nature et de l'utilisation du
matériel roulant.

### 29.1. Cotisation sur les motrices de métro (ligne 174) = nombre x tarif

### 29.2. Cotisation sur les remorques de métro (ligne 175) = nombre x tarif

### 29.3. Cotisation sur les automotrices et autres motrices (ligne 176) = nombre x tarif

### 29.4. Cotisation sur les autres remorques (ligne 177) = nombre x tarif

### 29.5. Total des cotisations (case 178) = somme des cotisations de chaqye type de matériel précédemment déterminées (ligne 174) + (ligne 175) + (ligne 176) + (ligne 177)

### 29.6. Frais de gestion (case 179)

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 29.7. Total de l'IFER sur le matériel roulant utilisé sur les lignes de transport en commun de voyageurs en Ile-de-France (ligne 180) = total des cotisations (case 178) + frais de gestion (case 179)

Ce montant est compris dans le total mentionné en ligne 192.

## 30. Imposition sur les répartiteurs principaux de la boucle locale cuivre et sur les réseaux de communications électroniques en fibre optique et en câble coaxial
-----------------------------------------------------------------------------------------------------------------------------------------------------------------

Le montant de l'imposition est fonction du nombre de lignes en service
que l'équipement comporte au 1^er^ janvier de l'année d'imposition.

### 30.1. Cotisation sur les répartiteurs principaux de la boucle locale cuivre (ligne 181) = nombre de lignes x tarif

### 30.2. Cotisation sur les points de mutualisation des réseaux en fibre optique (ligne 182) = nombre de lignes x tarif

### 30.3. Cotisation sur les nœuds de raccordement optique en câble coaxial (ligne 183) = nombre de lignes x tarif

### 30.4. Cotisation totale (case 184) = somme des cotisations sur chaque type d'équipement précédemment déterminées (ligne 181) + (ligne 182) + (ligne 183)

### 30.5. Frais de gestion (case 185)

Des frais de gestion sont perçus par l'État pour sa mission de calcul et
de recouvrement de l'IFER. Ces frais s'élèvent à 3 % de la cotisation
d'IFER.

### 30.6. Total de l'IFER sur les équipements télécom (ligne 186) = cotisation totale (case 184) + frais de gestion (case 185)

Ce montant est compris dans le total mentionné en ligne 192.

# V. CALCUL DU MONTANT A PAYER
## 31. Total des cotisations dues (ligne 192) 
-------------------------------------------

Le total des cotisations dues est égal à :

cotisation de CFE (ligne 25)

\+ cotisation de TCCI (ligne 35)

\+ cotisation de TCMA (ligne 47)

\+ cotisation d'IFER sur éoliennes et hydroliennes (ligne 60)

\+ cotisation d'IFER sur centrales nucléaires ou thermiques à flamme
(ligne 68)

\+ cotisation d'IFER sur centrales de production d'énergie électrique
d'origine photovoltaïque et hydraulique (ligne 82)

\+ cotisation d'IFER sur transformateurs électriques (ligne 93)

\+ cotisation d'IFER sur stations radioélectriques (ligne 117)

\+ cotisation d'IFER sur installations gazières et canalisations de
transport (ligne 152)

\+ cotisation d'IFER sur installations de production d'électricité
d'origine géothermique (ligne 158)

\+ cotisation d'IFER sur matériel roulant ferroviaire pour le transport
de voyageurs (ligne 173)

\+ cotisation d'IFER sur matériel roulant utilisé sur les lignes de
transport de voyageurs en Ile-de-France (ligne 180)

\+ cotisation d'IFER sur équipements de télécom (ligne 186)

## 32. Crédit d'impôt (ligne 193)
------------------------------

Les redevables de la CFE et les entreprises qui en sont temporairement
exonérées peuvent bénéficier d'un crédit d'impôt égal à 750 € par
salarié employé dans certains établissements situés dans une zone de
restructuration de la défense. Lorsque le crédit d'impôt est supérieur
au total des cotisations indiqué ligne 192, le montant non imputé est
remboursé s'il est supérieur à 7 €.

## 33. Montant de votre impôt (ligne 194)
--------------------------------------

Le montant de l'impôt est égal à :

total des cotisations dues (ligne 192)

\- crédit d'impôt (ligne 193)

Le montant net de CFE - IFER n'est pas réclamé lorsqu'il est inférieur
à 12 €.

[^1]: Taxe pour la gestion des milieux aquatiques et la prévention des
    inondations
