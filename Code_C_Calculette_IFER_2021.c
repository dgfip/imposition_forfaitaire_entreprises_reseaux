/*
 * Copyright DGFiP 2021
 * Ce logiciel est régi par la licence CeCILL-2.1 soumise au droit français. 
 * Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les 
 * conditions de la licence CeCILL-2.1 telle que diffusée par le CEA, le CNRS 
 * et l'INRIA sur le site : "http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.txt"
 */



#include <stdio.h>
#include <math.h>
#include <string.h>


#include "ifer-struc.h"

#define	TRUE	1
#define	FALSE	0



static double arrondi (double donn)
{
	donn=floor(donn + 0.5);
	return donn;
}



short ifercalc2021( struct e1 *itaux,struct e2 *ibase,struct e3 *iindic,struct s1 *icotis, FILE *efrescc)


{
int valeur;
double  zttpro;
short	trace			=	FALSE;
short	corse			=	FALSE;
short	alsace_moselle	=	FALSE;
short	depdom			=	FALSE;
short	moselle			=	FALSE;
short	grand_lyon   	=	FALSE;
short	st_mart     	=	FALSE;
short   non_auto_entr   =   FALSE;
double  Teolienne       =   7.7;
double  Tnucleaire      =   3206;
double  Thydraulique    =   3.206;
double  Ttransfo3       =   152445;
double  Ttransfo2       =   51734;
double  Ttransfo1       =   14859;
double  Tinstalgaz1     =   2751737;
double  Tinstalgaz8     =   603600;
double  Tstockgaz       =   550347;
double  Tcanalgaz       =   550;
double  Tstationcomp    =   110069;
double  Tcanalchim      =   534;
double  Tgeothermie     =   20.12;
double  Tlocodies       =   33021;
double  Tmotrice        =   25316;
double  Tlocoelec       =   22015;
double  Tmottgv         =   38526;
double  Tautomot        =   12660;
double  Twagon          =   5284;
double  Twagontgv       =   11007;
double  Twagontt        =   2641;
double  Tmetro          =   13495;
double  Tautometro      =   25316;
double  Trepartiteur    =   14.83;



if (strncmp(iindic->c_csdi,"2A0",sizeof(iindic->c_csdi)) == 0 ||
	strncmp(iindic->c_csdi,"2B0",sizeof(iindic->c_csdi)) == 0 )
{
	corse=TRUE;
}

if (strncmp(iindic->c_csdi,"971",sizeof(iindic->c_csdi)) == 0 &&
	strncmp(iindic->c_ctcn,"127",sizeof(iindic->c_ctcn)) == 0 )
{
	st_mart=TRUE;
}

if (strncmp(iindic->c_csdi,"690",sizeof(iindic->c_csdi)) == 0 &&
	strncmp(iindic->c_ctcngr,"300",sizeof(iindic->c_ctcngr)) == 0 )
{
	grand_lyon=TRUE;
}

trace = (efrescc && iindic &&  1 == iindic->s_zttpzz);

if (strncmp(iindic->c_csdi,"570",sizeof(iindic->c_csdi))==0 ||
	strncmp(iindic->c_csdi,"670",sizeof(iindic->c_csdi))==0 ||
	strncmp(iindic->c_csdi,"680",sizeof(iindic->c_csdi))==0 )
{
	alsace_moselle=TRUE;
}

if (strncmp(iindic->c_csdi,"570",sizeof(iindic->c_csdi))==0 )
{
	moselle=TRUE;
}

if (strncmp(iindic->c_csdi,"971",sizeof(iindic->c_csdi))==0 ||
	strncmp(iindic->c_csdi,"972",sizeof(iindic->c_csdi))==0 ||
	strncmp(iindic->c_csdi,"973",sizeof(iindic->c_csdi))==0 ||
	strncmp(iindic->c_csdi,"974",sizeof(iindic->c_csdi))==0 )
{
	depdom=TRUE;
}


if (trace)
{
	fprintf (efrescc," [- IFERCALC2021.C debut -]\n");
	fprintf (efrescc," ************************\n");
	if (grand_lyon)
	{
		fprintf (efrescc,"GRAND LYON\n");
		fprintf (efrescc,"DIRECTION\n");
		fprintf (efrescc,"		c_csdi =%s\n",iindic->c_csdi);
		fprintf (efrescc,"CTCNGR\n");
		fprintf (efrescc,"		c_ctcngr =%s\n",iindic->c_ctcngr);
	}
	else
    {
		fprintf (efrescc,"PAS GRAND LYON\n");
		fprintf (efrescc,"DIRECTION\n");
		fprintf (efrescc,"		c_csdi =%s\n",iindic->c_csdi);
		fprintf (efrescc,"CTCNGR\n");
		fprintf (efrescc,"		c_ctcngr =%s\n",iindic->c_ctcngr);
	}
}



	if (trace)
	{
		fprintf (efrescc,"	* 20. - IFER  -  \n");
		fprintf (efrescc,"		*****************\n");
		fprintf (efrescc,"		*** INDIC RG OU RS ***\n");
		fprintf (efrescc,"		 S_ZTTPRS=%d\n",iindic->s_zttprs);
		fprintf (efrescc,"		***\n");
		fprintf (efrescc,"		*** INDICATEURS***\n");
		fprintf (efrescc,"		*****************\n");
		fprintf (efrescc,"		*** EPCI VALIDE ***\n");
		fprintf (efrescc,"		 C_CICUVA=%s\n",itaux->c_cicuva);
		fprintf (efrescc,"		***\n");
		fprintf (efrescc,"		*** ZDE VALIDE  ***\n");
		fprintf (efrescc,"	     C_CICUZE=%s\n",itaux->c_cicuze);
		fprintf (efrescc,"		***\n");
		fprintf (efrescc,"		*** FPA/FPU     ***\n");
		fprintf (efrescc,"		 C_CICUU=%s\n",itaux->c_cicuu);
		fprintf (efrescc,"		***\n");
		fprintf (efrescc,"		*** ZONE        ***\n");	
		fprintf (efrescc,"		 C_CIETZ=%s\n",iindic->c_cietz);
		fprintf (efrescc,"		*****************\n");
	}
	if (trace)
	{
		fprintf (efrescc,"		***  1519 D**** \n");	
		fprintf (efrescc,"		* EOLIENNE (D-ZTTPEO) \n");	
		fprintf (efrescc,"		* puissance \n");	
		fprintf (efrescc,"		 D_ZTETEO=%.0f\n",ibase->d_zteteo);
	}
	if ((ibase->d_zteteo < 100)  && (iindic->s_zttprs == 0))
	{
		icotis->d_zttpeo = 0;
	}
	if ((ibase->d_zteteo >= 100)  || (iindic->s_zttprs == 1))
	{
		icotis->d_zttpeo=arrondi(ibase->d_zteteo*Teolienne);
	}
	if (trace)
	{
		fprintf (efrescc,"		 d_zttpeo=arrondi(%.0f*%.2f) \n",ibase->d_zteteo,Teolienne);
		fprintf (efrescc,"		 D_ZTTPEO=%.0f\n",icotis->d_zttpeo);
		fprintf (efrescc,"		* REPARTITION EOLIENNE (D-ZTTPEO) \n");	
	}

	if (icotis->d_zttpeo > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcnd1 =(icotis->d_zttpeo*20);
	        icotis->d_ztcud1 = 0;
		    if (trace) fprintf (efrescc,"		*commune isolee  (c_cicuva not = (V))\n");
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcnd1 = arrondi(icotis->d_zttpeo*20);
			icotis->d_ztcud1 = arrondi(icotis->d_zttpeo*50);
			if (trace) fprintf (efrescc,"		*commune membre d un EPCI : A dans CICUU \n");	
		}
		if ((strncmp(itaux->c_cicuu,"U",sizeof(itaux->c_cicuu)) == 0) ||
			(strncmp(itaux->c_cicuze, "V",sizeof(itaux->c_cicuze)) == 0) ||
			(strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			valeur =	strncmp(itaux->c_daetre,"20190101",sizeof(itaux->c_daetre));
			if (valeur < 0)
			{
				icotis->d_ztcnd1 = 0;
				if (grand_lyon)
				{
					icotis->d_ztcud1 = arrondi (icotis->d_zttpeo * 100);
				}
				else
				{
					icotis->d_ztcud1 = arrondi(icotis->d_zttpeo*70);
				}
			}
			else
			{
				icotis->d_ztcnd1=arrondi((icotis->d_zttpeo*itaux->d_zxcneo)/100);
	      		icotis->d_ztcud1=arrondi((icotis->d_zttpeo*itaux->d_zxcueo)/100);
			}
			if (trace)
			{
				fprintf (efrescc,"		*commune membre d un EPCI FPU/FPE  \n");
				fprintf (efrescc,"       * date raccordement : %s\n",itaux->c_daetre);
			}
		}

		icotis->d_ztdpd1 = arrondi (((icotis->d_zttpeo*100) - (icotis->d_ztcnd1 + icotis->d_ztcud1)));

		if (trace)
		{
			if (grand_lyon)
			{
				fprintf (efrescc,"		Grand LYON \n");
			}
			fprintf (efrescc,"		*part communale \n");	
			fprintf (efrescc,"		 D_ZTCND1=%.0f\n",icotis->d_ztcnd1);
			fprintf (efrescc,"		*Part epci \n");		
			fprintf (efrescc,"		 D_ZTCUD1=%.0f\n",icotis->d_ztcud1);
			fprintf (efrescc,"		*part departementale (D_ZTDPD1) \n");
			fprintf (efrescc,"		 d_ztdpd1=arrondi((%.0f*100)-(%.0f+%0f))\n",icotis->d_zttpeo,icotis->d_ztcnd1,icotis->d_ztcud1);
			fprintf (efrescc,"		 D_ZTDPD1=%.0f\n",icotis->d_ztdpd1);
		}
	}

	if (trace)
	{
		fprintf (efrescc,"		* HYDROLIENNE (D-ZTTPHY) \n");	
		fprintf (efrescc,"		* puissance \n");	
		fprintf (efrescc,"		 D_ZTETHY=%.0f\n",ibase->d_ztethy);
	}
	if (ibase->d_ztethy < 1000  && iindic->s_zttprs == 0)
	{			
		icotis->d_zttphy = 0;
	}
	if ((ibase->d_ztethy >= 1000)  || (iindic->s_zttprs == 1))
	{
		icotis->d_zttphy=arrondi(ibase->d_ztethy*Teolienne/10);
		if (trace)
		{
			fprintf (efrescc,"		 d_zttphy=arrondi(%.0f*%.2f/10) \n",ibase->d_ztethy,Teolienne);
			fprintf (efrescc,"		 D_ZTTPHY=%.0f\n",icotis->d_zttphy);
			fprintf (efrescc,"		*REPARTITION HYDROLIENNE (D-ZTTPHY) \n");	
		}
	}
	if (icotis->d_zttphy > 0)
	{
		if ((strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0) || (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0))
		{
			icotis->d_ztcnd2 =arrondi(icotis->d_zttphy*50);
		    icotis->d_ztcud2 = 0;
		    if (trace) fprintf (efrescc,"		*commune isolee ou epci a FPA   (c_cicuva not = (V)) et (c_cicuu = (A))\n");	
		}
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (strncmp(itaux->c_cicuze, "V",sizeof(itaux->c_cicuze)) == 0) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcnd2 = 0;
	        icotis->d_ztcud2 = arrondi (icotis->d_zttphy*50);
		    if (trace)
			{
				fprintf (efrescc,"		*commune membre d un EPCI a FPU : U dans CICUU \n");
				fprintf (efrescc,"		*        (ou FPE : V dans cicuze,O dans cietz  \n");
			}
		}

		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");	
			fprintf (efrescc,"		 D_ZTCND2=%.0f\n",icotis->d_ztcnd2);
		}

		if ((!grand_lyon) && (trace))
		{
			fprintf (efrescc,"		*Part epci \n");		
			fprintf (efrescc,"		 D_ZTCUD2=%.0f\n",icotis->d_ztcud2);
		}

		icotis->d_ztdpd2 = arrondi((icotis->d_zttphy*100) - (icotis->d_ztcnd2 + icotis->d_ztcud2));
		if (grand_lyon)
		{
			icotis->d_ztcud2 = arrondi (icotis->d_zttphy * 100);
			icotis->d_ztdpd2 = 0;
			icotis->d_ztcnd2 = 0;
			if (trace)
			{
				fprintf (efrescc,"		Grand LYON \n");
				fprintf (efrescc,"		*PART EPCI RECALCULEE (montant IFER(ZTTPHY) * 100) \n");			
				fprintf (efrescc,"		 D_ZTTPHY=%.0f\n",icotis->d_zttphy);
				fprintf (efrescc,"		 D_ZTCUD2=%.0f\n",icotis->d_ztcud2);
				fprintf (efrescc,"		*part departementale definitive (D_ZTDPD2) \n");	
				fprintf (efrescc,"		 D_ZTDPD2=%.0f\n",icotis->d_ztdpd2);
				fprintf (efrescc,"		*part communale definitive (D_ZTCND2) \n");	
				fprintf (efrescc,"		 D_ZTCND2=%.0f\n",icotis->d_ztcnd2);
			}
		}
		else
        {
			if (trace)
			{
				fprintf (efrescc,"		*part departementale (D_ZTDPD2) \n");
				fprintf (efrescc,"		 d_ztdpd2=arrondi((%.0f*100)-(%.0f+%0f))\n",icotis->d_zttphy,icotis->d_ztcnd2,icotis->d_ztcud2);
				fprintf (efrescc,"		 D_ZTDPD2=%.0f\n",icotis->d_ztdpd2);
			}
		}
	}

	icotis->d_zttpe1=arrondi((icotis->d_zttpeo+icotis->d_zttphy)*0.01);
	icotis->d_zttpe2=arrondi((icotis->d_zttpeo+icotis->d_zttphy)*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion 1519D \n");	
		fprintf (efrescc,"		*FAR 1519D 1%% \n");
		fprintf (efrescc,"		 D_ZTTPE1=arrondi(%.0f x 0.01)\n",icotis->d_zttpeo,icotis->d_zttphy);
		fprintf (efrescc,"		*FDNV 1519D 2%% \n");
		fprintf (efrescc,"		 D_ZTTPE2=arrondi(%.0f x 0.02)\n",icotis->d_zttpeo,icotis->d_zttphy);
	}

	if (trace)
	{
		fprintf (efrescc,"		***  1519 E**** \n");	
		fprintf (efrescc,"		*CENTRALE NUCLEAIRE (D-ZTTPE) \n");	
		fprintf (efrescc,"		* puissance \n");	
		fprintf (efrescc,"		 D_ZTTPNU=%.0f\n",ibase->d_zttpnu);
	}
	if ((ibase->d_zttpnu < 50) && (iindic->s_zttprs == 0))
	{
		icotis->d_zttpe = 0;
	}
	if ((ibase->d_zttpnu >= 50) || (iindic->s_zttprs == 1))
	{
	    icotis->d_zttpe=arrondi(ibase->d_zttpnu*Tnucleaire);
	}
	if (trace)
	{
		fprintf (efrescc,"		 d_zttpe=arrondi(%.0f*%.0f) \n",ibase->d_zttpnu,Tnucleaire);
		fprintf (efrescc,"		 D_ZTTPE=%.0f\n",icotis->d_zttpe);
	}

	if (icotis->d_zttpe > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcne1 = arrondi(icotis->d_zttpe*50);
	        icotis->d_ztcue1 = 0;
			icotis->d_ztcnfp = arrondi(icotis->d_ztcne1*2/100);
			icotis->d_ztcne1 = icotis->d_ztcne1 - icotis->d_ztcnfp;
			if (trace) fprintf (efrescc,"		*commune isolee  (c_cicuva not = (V))\n");
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcne1=arrondi((icotis->d_zttpe*itaux->d_ztcnnu)/100);
	      	icotis->d_ztcue1=arrondi((icotis->d_zttpe*itaux->d_ztcnna)/100);
			icotis->d_ztcnfp = arrondi(icotis->d_ztcne1*2/100) + arrondi(icotis->d_ztcue1*2/100);
			icotis->d_ztcne1 = icotis->d_ztcne1 - arrondi(icotis->d_ztcne1*2/100);
			icotis->d_ztcue1 = icotis->d_ztcue1 - arrondi(icotis->d_ztcue1*2/100);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI : A dans CICUU \n");
		}
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcne1 = 0;
			icotis->d_ztcue1 = arrondi (icotis->d_zttpe*50);
			icotis->d_ztcnfp = arrondi(icotis->d_ztcue1*2/100);
			icotis->d_ztcue1 = icotis->d_ztcue1 - icotis->d_ztcnfp;
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI   \n");	
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");
			fprintf (efrescc,"		 D_ZTCNE1=%.0f\n",icotis->d_ztcne1);
		}
	   if ((!grand_lyon) && (trace))
	   {
			fprintf (efrescc,"		*Part epci \n");		
			fprintf (efrescc,"		 D_ZTCUE1=%.0f\n",icotis->d_ztcue1);
	   }

		icotis->d_ztdpe1 = arrondi((icotis->d_zttpe*100) - (icotis->d_ztcne1 + icotis->d_ztcue1 + icotis->d_ztcnfp));
		if (grand_lyon)
		{
			icotis->d_ztcue1 = arrondi (icotis->d_zttpe * 100);
		    icotis->d_ztdpe1 = 0;
		    icotis->d_ztcne1 = 0;
			icotis->d_ztcnfp = 0;
			if (trace)
			{
				fprintf (efrescc,"		*PART EPCI RECALCULEE (montant IFER(ZTTPE) * 100) \n");		
				fprintf (efrescc,"		 D_ZTTPE=%.0f\n",icotis->d_zttpe);
				fprintf (efrescc,"		 D_ZTCUE1=%.0f\n",icotis->d_ztcue1);
				fprintf (efrescc,"		*part departementale definitive(D_ZTDPE1) \n");	
				fprintf (efrescc,"		 D_ZTDPE1=%.0f\n",icotis->d_ztdpe1);
				fprintf (efrescc,"		*part communale definitive(D_ZTCNE1) \n");	
				fprintf (efrescc,"		 D_ZTCNE1=%.0f\n",icotis->d_ztcne1);
				fprintf (efrescc,"		*fond de compensation(D_ZTCNFP) \n");	
				fprintf (efrescc,"		 D_ZTCNFP=%.0f\n",icotis->d_ztcnfp);
			}
		}
		else
		{
			if (trace)
			{
				fprintf (efrescc,"		*fond de compensation(D_ZTCNFP) \n");	
				fprintf (efrescc,"		 D_ZTCNFP=%.0f\n",icotis->d_ztcnfp);
				fprintf (efrescc,"		*part departementale (D_ZTDPE1) \n");
				fprintf (efrescc,"		 d_ztdpe1=arrondi((%.0f*100)-(%.0f+%0f+%0f))\n",icotis->d_zttpe,icotis->d_ztcne1,icotis->d_ztcue1,icotis->d_ztcnfp);
				fprintf (efrescc,"		 D_ZTDPE1=%.0f\n",icotis->d_ztdpe1);
			}
		}
	}

	icotis->d_zttpn1=arrondi(icotis->d_zttpe*0.01);
	icotis->d_zttpn2=arrondi(icotis->d_zttpe*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion 1519E \n");	
		fprintf (efrescc,"		*FAR 1519E 1%% \n");
		fprintf (efrescc,"		 D_ZTTPN1=arrondi(%.0f x 0.01)\n",icotis->d_zttpe);
		fprintf (efrescc,"		 D_ZTTPN1=%.0f\n",icotis->d_zttpn1);
		fprintf (efrescc,"		*FDNV 1519E 2%% \n");
		fprintf (efrescc,"		 D_ZTTPN2=arrondi(%.0f x 0.02)\n",icotis->d_zttpe);
		fprintf (efrescc,"		 D_ZTTPN2=%.0f\n",icotis->d_zttpn2);
	}

	if  (ibase->d_zxetch == 0)
	{
		ibase->d_zxetch = 10000;
	}
	if ((((ibase->d_ztetch / ibase->d_zxetch) * 100) >= 1) || iindic->s_zttprs == 1)
	{
		icotis->d_zttpch=arrondi(ibase->d_ztetch*Thydraulique);
	}
	else
	{	
		icotis->d_zttpch = 0;
	}

	if (trace)
	{
		fprintf (efrescc,"		***  1519 F**** \n");	
		fprintf (efrescc,"		* CENTRALE HYDRAULIQUE (D-ZTTPCH) \n");	
		fprintf (efrescc,"		* puissance \n");	
		fprintf (efrescc,"		 D_ZXETCH=%.0f\n",ibase->d_zxetch);
		fprintf (efrescc,"		*D_ZXETCH not =  0 et (D_ZTETCH / D_ZXETCH) < 1  \n");	
		fprintf (efrescc,"		 d_ztetch=%.0f \n",ibase->d_ztetch);
		fprintf (efrescc,"		 d_zttpch=arrondi(%.0f*%.3f) \n",ibase->d_ztetch,Thydraulique);
		fprintf (efrescc,"		 D_ZTTPCH=%.0f\n",icotis->d_zttpch);
		fprintf (efrescc,"		* REPARTITION CENTRALE HYDRAULIQUE (D-ZTTPCH) \n");
	}

	if (icotis->d_zttpch > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcnf2 = arrondi (icotis->d_zttpch*50);
	        icotis->d_ztcuf2 = 0;
		    if (trace) fprintf (efrescc,"		*commune isolee  (c_cicuva not = (V))\n");	
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcnf2=arrondi((icotis->d_zttpch*itaux->d_zxcnph)/100);
	      	icotis->d_ztcuf2=arrondi((icotis->d_zttpch*itaux->d_zxcnpa)/100);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI : A dans CICUU \n");
		}
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcnf2 = 0;
			icotis->d_ztcuf2 = arrondi (icotis->d_zttpch*50);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI   \n");	
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");
			fprintf (efrescc,"		 D_ZTCNF2=%.0f\n",icotis->d_ztcnf2);
		}
		if (!grand_lyon)
		{
			if (trace)
			{
				fprintf (efrescc,"		*Part epci \n");		
				fprintf (efrescc,"		 D_ZTCUF2=%.0f\n",icotis->d_ztcuf2);
			}
		}

		icotis->d_ztdpf2 = arrondi ((icotis->d_zttpch*100) - (icotis->d_ztcnf2 + icotis->d_ztcuf2));
		if (grand_lyon)
		{
			icotis->d_ztcuf2 = arrondi (icotis->d_zttpch * 100);
			icotis->d_ztdpf2 = 0;
			icotis->d_ztcnf2 = 0;
			if (trace)
			{
				fprintf (efrescc,"		*PART EPCI RECALCULEE (montant IFER(ZTTPCH) * 100) \n");		
				fprintf (efrescc,"		 D_ZTTPCH=%.0f\n",icotis->d_zttpch);
				fprintf (efrescc,"		 D_ZTCUF2=%.0f\n",icotis->d_ztcuf2);
				fprintf (efrescc,"		*part departementale definitive (D_ZTDPF2) \n");	
				fprintf (efrescc,"		 D_ZTDPF2=%.0f\n",icotis->d_ztdpf2);
				fprintf (efrescc,"		*part communale definitive (D_ZTCNF2) \n");	
				fprintf (efrescc,"		 D_ZTCNF2=%.0f\n",icotis->d_ztcnf2);
			}
		}
		else
        {
			if (trace)
			{
				fprintf (efrescc,"		*part departementale (D_ZTDPF2) \n");
				fprintf (efrescc,"		 d_ztdpf2=arrondi((%.0f*100)-(%.0f+%0f))\n",icotis->d_zttpch,icotis->d_ztcnf2,icotis->d_ztcuf2);
				fprintf (efrescc,"		 D_ZTDPF2=%.0f\n",icotis->d_ztdpf2);
			}
		}
	}

	if ((ibase->d_ztetcp < 100) && (iindic->s_zttprs == 0))
	{
		icotis->d_zttpph = 0;
	}
	if ((ibase->d_ztetcp >= 100) || (iindic->s_zttprs == 1))
	{
		icotis->d_zttpph=arrondi(ibase->d_ztetcp*Teolienne);
	}
	if (trace)
	{
		fprintf (efrescc,"		*CENTRALE PHOTOVOLTAIQUE (D-ZTTPPH) \n");	
		fprintf (efrescc,"		* puissance \n");	
		fprintf (efrescc,"		 D_ZTETCP=%.0f\n",ibase->d_ztetcp);
		fprintf (efrescc,"		 d_zttpph=arrondi(%.0f*%.2f) \n",ibase->d_ztetcp,Teolienne);
		fprintf (efrescc,"		 D_ZTTPPH=%.0f\n",icotis->d_zttpph);
	}

	if (icotis->d_zttpph > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcnf1= arrondi(icotis->d_zttpph*50);
	        icotis->d_ztcuf1 = 0;
		    if (trace) fprintf (efrescc,"		*commune isolee  (c_cicuva not = (V))\n");
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcnf1=arrondi((icotis->d_zttpph*itaux->d_zxcnph)/100);
	      	icotis->d_ztcuf1=arrondi((icotis->d_zttpph*itaux->d_zxcnpa)/100);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI : A dans CICUU \n");	
		}
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcnf1 = 0;
			icotis->d_ztcuf1 = arrondi (icotis->d_zttpph*50);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI   \n");
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");
			fprintf (efrescc,"		 d_ztcnf1=arrondi(%.0f*50) \n",icotis->d_zttpph);
			fprintf (efrescc,"		 D_ZTCNF1=%.0f\n",icotis->d_ztcnf1);
		}

		if (!grand_lyon)
		{
			if (trace)
			{
				fprintf (efrescc,"		*Part epci \n");		
				fprintf (efrescc,"		 D_ZTCUF1=%.0f\n",icotis->d_ztcuf1);
			}
		}

		icotis->d_ztdpf1 = arrondi ((icotis->d_zttpph*100) - (icotis->d_ztcnf1 + icotis->d_ztcuf1));
		if (grand_lyon)
		{
			icotis->d_ztcuf1 = arrondi (icotis->d_zttpph * 100);
			icotis->d_ztdpf1 = 0;
			icotis->d_ztcnf1 = 0;
			if (trace)
			{
				fprintf (efrescc,"		*PART EPCI RECALCULEE (montant IFER(ZTTPPH) * 100) \n");				
				fprintf (efrescc,"		 D_ZTTPPH=%.0f\n",icotis->d_zttpph);
				fprintf (efrescc,"		 D_ZTCUF1=%.0f\n",icotis->d_ztcuf1);
				fprintf (efrescc,"		*part departementale defintive (D_ZTDPF1) \n");	
				fprintf (efrescc,"		 D_ZTDPF1=%.0f\n",icotis->d_ztdpf1);
				fprintf (efrescc,"		*part communale defintive (D_ZTCNF1) \n");	
				fprintf (efrescc,"		 D_ZTCNF1=%.0f\n",icotis->d_ztcnf1);
			}
		}
		else
        {
			if (trace)
			{
				fprintf (efrescc,"		*part departementale (D_ZTDPF1) \n");
				fprintf (efrescc,"		 d_ztdpf1=arrondi((%.0f*100)-(%.0f+%0f))\n",icotis->d_zttpph,icotis->d_ztcnf1,icotis->d_ztcuf1);
				fprintf (efrescc,"		 D_ZTDPF1=%.0f\n",icotis->d_ztdpf1);
			}
		}
	}

	icotis->d_zttph1=arrondi((icotis->d_zttpch+icotis->d_zttpph)*0.01);
	icotis->d_zttph2=arrondi((icotis->d_zttpch+icotis->d_zttpph)*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion 1519F \n");	
		fprintf (efrescc,"		*FAR 1519F 1%% \n");
		fprintf (efrescc,"		 D_ZTTPH1=arrondi((%.0f+%.0f) x 0.01)\n",icotis->d_zttpch,icotis->d_zttpph);
		fprintf (efrescc,"		*FDNV 1519F 2% \n");
		fprintf (efrescc,"		 D_ZTTPH1=arrondi((%.0f+%.0f) x 0.02)\n",icotis->d_zttpch,icotis->d_zttpph);
	}

	icotis->d_zttpt1 = arrondi((ibase->d_ztett1*Ttransfo1) / 100);
	icotis->d_zttpt2 = arrondi((ibase->d_ztett2*Ttransfo2) / 100);
	icotis->d_zttpt3 = arrondi((ibase->d_ztett3*Ttransfo3) / 100);
	if (trace)
	{
		fprintf (efrescc,"		***  1519 G **** \n");
		fprintf (efrescc,"		*TRANSFORMATEUR ELECTRIQUE  - Transformateur 1\n");
		fprintf (efrescc,"		 d_zttpt1=(arrondi(%.0f*%.0f) \n",ibase->d_ztett1,Ttransfo1);
		fprintf (efrescc,"		 D_ZTTPT1=%.0f\n",icotis->d_zttpt1);
		fprintf (efrescc,"		*TRANSFORMATEUR ELECTRIQUE  - Transformateur 2\n");
		fprintf (efrescc,"		 d_zttpt2=(arrondi(%.0f*%.0f) \n",ibase->d_ztett2,Ttransfo2);
		fprintf (efrescc,"		 D_ZTTPT2=%.0f\n",icotis->d_zttpt2);
		fprintf (efrescc,"		*TRANSFORMATEUR ELECTRIQUE  - Transformateur 3\n");
		fprintf (efrescc,"		 d_zttpt3=(arrondi(%.0f*%.0f) \n",ibase->d_ztett3,Ttransfo3);
		fprintf (efrescc,"		 D_ZTTPT3=%.0f\n",icotis->d_zttpt3);
	}

	if ((icotis->d_zttpt1 + icotis->d_zttpt2 + icotis->d_zttpt3) > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcnte = arrondi ((icotis->d_zttpt1 + icotis->d_zttpt2 + icotis->d_zttpt3 ) * 100);
	        icotis->d_ztcute = 0;
		    if (trace) fprintf (efrescc,"		*commune isolee  (c_cicuva not = (V))\n");	
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcnte=arrondi(((icotis->d_zttpt1+icotis->d_zttpt2+icotis->d_zttpt3)*itaux->d_zxcnte)/100);
			icotis->d_ztcute=arrondi(((icotis->d_zttpt1+icotis->d_zttpt2+icotis->d_zttpt3)*itaux->d_ztcnta)/100);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI : A dans CICUU \n");
		}	
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (grand_lyon) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcnte = 0;
			icotis->d_ztcute = arrondi ((icotis->d_zttpt1 + icotis->d_zttpt2 + icotis->d_zttpt3 ) * 100);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI   \n");
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");	
			fprintf (efrescc,"		 D_ZTCNTE=%.0f\n",icotis->d_ztcnf1);
			fprintf (efrescc,"		*part epci \n");
			fprintf (efrescc,"		 D_ZTCUTE=%.0f\n",icotis->d_ztcute);
		}
	}

	icotis->d_zztpt1=arrondi((icotis->d_zttpt1+icotis->d_zttpt2+icotis->d_zttpt3)*0.01);
	icotis->d_zztpt2=arrondi((icotis->d_zttpt1+icotis->d_zttpt2+icotis->d_zttpt3)*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion 1519G \n");	
		fprintf (efrescc,"		*FAR 1519G 1%% \n");
		fprintf (efrescc,"		 D_ZZTPT1=arrondi((%.0f+%.0f+%.0f) x 0.01)\n",icotis->d_zttpt1,icotis->d_zttpt2,icotis->d_zttpt3);
		fprintf (efrescc,"		*FDNV 1519G 2%% \n");
		fprintf (efrescc,"		 D_ZZTPT2=arrondi((%.0f+%.0f+%.0f) x 0.02)\n",icotis->d_zttpt1,icotis->d_zttpt2,icotis->d_zttpt3);
	}
	
	if (trace)
	{
		fprintf (efrescc,"		***  1519 H   **** \n");
		fprintf (efrescc,"		*STATION RADIO ELECTRIQUES* \n");
		fprintf (efrescc,"		*montant station radio \n");
		fprintf (efrescc,"		 D_ZTTPSI=%.0f\n",ibase->d_zttpsi);
	}
	if ((ibase->d_zttpsi > 0) && (iindic->s_zttprs == 0))
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcnh1= arrondi((ibase->d_zttpsi*200)/3);
	        icotis->d_ztcuh1 = 0;
		    if (trace) fprintf (efrescc,"		*commune isolee  (c_cicuva not = (V))\n");	
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcnh1=arrondi((ibase->d_zttpsi*itaux->d_ztcnsr)/100);
			icotis->d_ztcuh1=arrondi((ibase->d_zttpsi*itaux->d_ztcnsa)/100);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI : A dans CICUU \n");	
		}
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcnh1 = 0;
			icotis->d_ztcuh1= arrondi((ibase->d_zttpsi*200)/3);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI   \n");
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");	
			fprintf (efrescc,"		 D_ZTCNH1=%.0f\n",icotis->d_ztcnh1);
		}

		if ((!grand_lyon) && (trace))
		{
			fprintf (efrescc,"		*Part epci \n");		
			fprintf (efrescc,"		 D_ZTCUH1=%.0f\n",icotis->d_ztcuh1);
		}

		icotis->d_ztdph1 =  arrondi((ibase->d_zttpsi*100) - (icotis->d_ztcnh1 + icotis->d_ztcuh1));
		if (grand_lyon)
		{
			icotis->d_ztcuh1 = arrondi(ibase->d_zttpsi * 100);
			icotis->d_ztdph1 = 0;
			icotis->d_ztcnh1 = 0;
			if (trace)
			{
				fprintf (efrescc,"		*PART EPCI RECALCULEE (montant IFER(ZTTPSI) * 100) \n");	
				fprintf (efrescc,"		 D_ZTTPSI=%.0f\n",ibase->d_zttpsi);
				fprintf (efrescc,"		 D_ZTCUH1=%.0f\n",icotis->d_ztcuh1);
				fprintf (efrescc,"		*part departementale definitive (D_ZTDPH1) \n");	
				fprintf (efrescc,"		 D_ZTDPH1=%.0f\n",icotis->d_ztdph1);
				fprintf (efrescc,"		*part communale definitive (D_ZTCNH1) \n");	
				fprintf (efrescc,"		 D_ZTCNH1=%.0f\n",icotis->d_ztcnh1);
			}
		}
		else
        {
			if (trace)
			{
				fprintf (efrescc,"		*part departementale (D_ZTDPF1) \n");
				fprintf (efrescc,"		 d_ztdph1=arrondi(%.0f*100)-(%.0f+%.0f) \n",ibase->d_zttpsi,icotis->d_ztcnh1, icotis->d_ztcuh1);
				fprintf (efrescc,"		 D_ZTDPH1=%.0f\n",icotis->d_ztdph1);
			}
		}
	}

	icotis->d_zttps1=arrondi(ibase->d_zttpsi*0.01);
	icotis->d_zttps2=arrondi(ibase->d_zttpsi*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion 1519H \n");	
		fprintf (efrescc,"		*FAR 1519H 1%% \n");
		fprintf (efrescc,"		 D_ZTTPS1=arrondi(%.0f  x 0.01)\n",ibase->d_zttpsi);
		fprintf (efrescc,"		*FDNV 1519H 2%% \n");
		fprintf (efrescc,"		 D_ZTTPS2=arrondi(%.0f  x 0.01)\n",ibase->d_zttpsi);
	}
	
	ibase->d_zztpg1 = (ibase->d_zztpg1/1000000000);
	icotis->d_zxtpg1 = (ibase->d_zztpg1*Tinstalgaz1);
	icotis->d_zxtpg1 = arrondi (icotis->d_zxtpg1/100);
	if (trace)
	{
		fprintf (efrescc,"		***  1519 HA **** \n");
		fprintf (efrescc,"		*Installation gaz > 100000\n");	
		fprintf (efrescc,"		 D_ZXTPG1=(%.0f)\n",icotis->d_zxtpg1);
	}
	ibase->d_zztpg8 = (ibase->d_zztpg8/1000000000);
	icotis->d_zxtpg8 = (ibase->d_zztpg8*Tinstalgaz8);
	icotis->d_zxtpg8 = arrondi (icotis->d_zxtpg8/100);
	if (trace)
	{
		fprintf (efrescc,"		*Installation gaz < 100000\n");	
		fprintf (efrescc,"		 D_ZXTPG8=(%.0f)\n",icotis->d_zxtpg8);
	}

	if ((icotis->d_zxtpg1 + icotis->d_zxtpg8) > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcug1 = 0;
			icotis->d_ztcng1=((icotis->d_zxtpg1+icotis->d_zxtpg8)*100);
		    if (trace) fprintf (efrescc,"		* commune isolee   \n");	
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcng1 = (((icotis->d_zxtpg1+icotis->d_zxtpg8)*itaux->d_zxcna1)/100);
			icotis->d_ztcug1=(((icotis->d_zxtpg1+icotis->d_zxtpg8)*itaux->d_zxcua1)/100);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI add  \n");	
		}
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (grand_lyon) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcng1 = 0;
			icotis->d_ztcug1=((icotis->d_zxtpg1+icotis->d_zxtpg8)*100);
			if (trace) fprintf (efrescc,"		*commune membre d un EPCI tpu  \n");
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");	
			fprintf (efrescc,"		 D_ZTCNG1=%.0f\n",icotis->d_ztcng1);
			fprintf (efrescc,"		*part epci \n");
			fprintf (efrescc,"		 D_ZTCUG1=%.0f\n",icotis->d_ztcug1);
		}
	}

	ibase->d_zztpg2 = (ibase->d_zztpg2/1000000000);
	icotis->d_zxtpg2 = (ibase->d_zztpg2*Tstockgaz);
	icotis->d_zxtpg2 = arrondi (icotis->d_zxtpg2/100);
	if (trace)
	{
		fprintf (efrescc,"		*Stockage gaz\n");
		fprintf (efrescc,"		 D_ZXTPG2=(%.0f)\n",icotis->d_zxtpg2);
	}

	if (icotis->d_zxtpg2 > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcug2 = 0;
			icotis->d_ztcng2=(icotis->d_zxtpg2*50);
		    if (trace) fprintf (efrescc,"		* commune isolee   \n");	
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcng2 = ((icotis->d_zxtpg2*itaux->d_zxcna2)/100);
			icotis->d_ztcug2=((icotis->d_zxtpg2*itaux->d_zxcua2)/100);
			if (trace) fprintf (efrescc,"		*commune membre d un EPCI add  \n");	
	    }
		if (strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcng2 = 0;
			icotis->d_ztcug2 = arrondi (icotis->d_zxtpg2*100);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI tpu  \n");	
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");	
			fprintf (efrescc,"		 D_ZTCNG2=%.0f\n",icotis->d_ztcng2);
		}

		if ((!grand_lyon) && (trace))
		{
			fprintf (efrescc,"		*Part epci \n");		
			fprintf (efrescc,"		 D_ZTCUG2=%.0f\n",icotis->d_ztcug2);
		}

		icotis->d_ztdpg2 = arrondi ((icotis->d_zxtpg2*100) - (icotis->d_ztcng2 + icotis->d_ztcug2));

		if ((grand_lyon) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcug2 = arrondi (icotis->d_zxtpg2 * 100);
			icotis->d_ztdpg2 = 0;
			icotis->d_ztcng2 = 0;
			if (trace)
			{
				fprintf (efrescc,"		*PART EPCI RECALCULEE (montant IFER(ZXTPG2) * 100) \n");
				fprintf (efrescc,"		 D_ZXTPG2=%.0f\n",icotis->d_zxtpg2);
				fprintf (efrescc,"		 D_ZTCUG2=%.0f\n",icotis->d_ztcug2);
				fprintf (efrescc,"		*part departementale definitive  (D_ZTDPG2) \n");	
				fprintf (efrescc,"		 D_ZTDPG2=%.0f\n",icotis->d_ztdpg2);
				fprintf (efrescc,"		*part communale definitive  (D_ZTCNG2) \n");	
				fprintf (efrescc,"		 D_ZTCNG2=%.0f\n",icotis->d_ztcng2);
			}
		}
		else
        {
			if (trace)
			{
				fprintf (efrescc,"		*part departementale (D_ZTDPG2) \n");
				fprintf (efrescc,"		 d_ztdpg2=arrondi((%.0f*100)-(%.0f+%0f))\n",icotis->d_zxtpg2,icotis->d_ztcng2,icotis->d_ztcug2);
				fprintf (efrescc,"		 D_ZTDPG2=%.0f\n",icotis->d_ztdpg2);
			}
        }
	}

	icotis->d_zxtpg3 = arrondi(ibase->d_zttpg3*Tcanalgaz/1000);
	if (trace)
	{
		fprintf (efrescc,"		*TRANSPORT GAZ\n");
		fprintf (efrescc,"		 d_zxtpg3=(arrondi(%.0f*%.0f/1000) \n",ibase->d_zttpg3,Tcanalgaz);
		fprintf (efrescc,"		 D_ZXTPG3=%.0f\n",icotis->d_zxtpg3);
	}

	if (icotis->d_zxtpg3 > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcug3 = 0;
			icotis->d_ztcng3=(icotis->d_zxtpg3*50);
		    if (trace) fprintf (efrescc,"		* commune isolee   \n");
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcng3 = ((icotis->d_zxtpg3*itaux->d_zxcna3)/100);
			icotis->d_ztcug3=((icotis->d_zxtpg3*itaux->d_zxcua3)/100);
			if (trace) fprintf (efrescc,"		*commune membre d un EPCI add  \n");	
	    }
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcng3 = 0;
			icotis->d_ztcug3 = arrondi (icotis->d_zxtpg3*50);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI tpu  \n");	
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");
			fprintf (efrescc,"		 d_ztcng3=(%.0f*50)\n",icotis->d_zxtpg3);
			fprintf (efrescc,"		 D_ZTCNG3=%.0f\n",icotis->d_ztcng3);
		}

		if ((!grand_lyon) && (trace))
		{
			if (trace)
			{
				fprintf (efrescc,"		*Part epci \n");		
				fprintf (efrescc,"		 D_ZTCUG3=%.0f\n",icotis->d_ztcug3);
			}
		}

		icotis->d_ztdpg3 =  ((icotis->d_zxtpg3*100) - (icotis->d_ztcng3 + icotis->d_ztcug3));
		if (grand_lyon)
		{
			icotis->d_ztcug3 = arrondi (icotis->d_zxtpg3 * 100);
			icotis->d_ztdpg3 = 0;
			icotis->d_ztcng3 = 0;
			if (trace)
			{
				fprintf (efrescc,"		*PART EPCI RECALCULEE (montant IFER(ZXTPG3) * 100) \n");
				fprintf (efrescc,"		 D_ZXTPG3=%.0f\n",icotis->d_zxtpg3);
				fprintf (efrescc,"		 D_ZTCUG3=%.0f\n",icotis->d_ztcug3);
				fprintf (efrescc,"		*part departementale definitive (D_ZTDPG3) \n");	
				fprintf (efrescc,"		 D_ZTDPG3=%.0f\n",icotis->d_ztdpg3);
				fprintf (efrescc,"		*part departementale definitive (D_ZTCNG3) \n");	
				fprintf (efrescc,"		 D_ZTCNG3=%.0f\n",icotis->d_ztcng3);
			}
		}
		else
        {
			if (trace)
			{
				fprintf (efrescc,"		*part departementale (D_ZTDPG3) \n");
				fprintf (efrescc,"		 d_ztdpg3=((%.0f*100)-(%.0f+%0f))\n",icotis->d_zxtpg3,icotis->d_ztcng3,icotis->d_ztcug3);
				fprintf (efrescc,"		 D_ZTDPG3=%.0f\n",icotis->d_ztdpg3);
			}
		}
	}

	ibase->d_zztpg4 = (ibase->d_zztpg4/1000000000);
	icotis->d_zxtpg4 = (ibase->d_zztpg4*Tstationcomp);
	icotis->d_zxtpg4 = arrondi (icotis->d_zxtpg4/100);
	if (trace)
	{
		fprintf (efrescc,"		*STATION DE COMPRESSION \n");
		fprintf (efrescc,"		 D_ZXTPG4=(%.0f)\n",icotis->d_zxtpg4);
	}

	if (icotis->d_zxtpg4 > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcug4 = 0;
			icotis->d_ztcng4=(icotis->d_zxtpg4*100);
		    if (trace) fprintf (efrescc,"		* commune isolee   \n");
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcng4 = ((icotis->d_zxtpg4*itaux->d_zxcna1)/100);
			icotis->d_ztcug4=((icotis->d_zxtpg4*itaux->d_zxcua1)/100);
			if (trace) fprintf (efrescc,"		*commune membre d un EPCI add  \n");	
		}
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (grand_lyon) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcng4 = 0;
			icotis->d_ztcug4=(icotis->d_zxtpg4*100);
			if (trace) fprintf (efrescc,"		*commune membre d un EPCI tpu  \n");	
		}

		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");	
			fprintf (efrescc,"		 D_ZTCNG4=%.0f\n",icotis->d_ztcng4);
			fprintf (efrescc,"		*part epci \n");
			fprintf (efrescc,"		 d_ztcug4=(%.0f*100)\n",icotis->d_zxtpg4);
			fprintf (efrescc,"		 D_ZTCUG4=%.0f\n",icotis->d_ztcug4);
		}
	}
	
	icotis->d_zxtpg5 = arrondi(ibase->d_zttpg5*Tcanalgaz/1000);
	if (trace)
	{
		fprintf (efrescc,"		*Transport hydrocarbure\n");
		fprintf (efrescc,"		 d_zxtpg5=(arrondi(%.0f*%.0f/1000) \n",ibase->d_zttpg5,Tcanalgaz);
		fprintf (efrescc,"		 D_ZXTPG5=%.0f\n",icotis->d_zxtpg5);
	}

	if (icotis->d_zxtpg5 > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcug5 = 0;
			icotis->d_ztcng5=(icotis->d_zxtpg5*50);
		    if (trace) fprintf (efrescc,"		* commune isolee   \n");
		}
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcng5 = ((icotis->d_zxtpg5*itaux->d_zxcna3)/100);
			icotis->d_ztcug5=((icotis->d_zxtpg5*itaux->d_zxcua3)/100);
			if (trace) fprintf (efrescc,"		*commune membre d un EPCI add  \n");	
		}
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcng5 = 0;
			icotis->d_ztcug5 = arrondi (icotis->d_zxtpg5*50);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI tpu  \n");	
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");
			fprintf (efrescc,"		 d_ztcng5=(%.0f*50)\n",icotis->d_zxtpg3);
			fprintf (efrescc,"		 D_ZTCNG5=%.0f\n",icotis->d_ztcng5);
		}

		if ((!grand_lyon) && (trace))
		{
			if (trace)
			{
				fprintf (efrescc,"		*Part epci \n");		
				fprintf (efrescc,"		 D_ZTCUG5=%.0f\n",icotis->d_ztcug5);
			}
		}

		icotis->d_ztdpg5 =  ((icotis->d_zxtpg5*100) - (icotis->d_ztcng5 + icotis->d_ztcug5));
		if (grand_lyon)
		{
			icotis->d_ztcug5 = arrondi (icotis->d_zxtpg5 * 100);
			icotis->d_ztdpg5 = 0;
			icotis->d_ztcng5 = 0;
			if (trace)
			{
				fprintf (efrescc,"		*PART EPCI RECALCULEE (montant IFER(ZXTPG5) * 100) \n");	
				fprintf (efrescc,"		 D_ZXTPG5=%.0f\n",icotis->d_zxtpg5);
				fprintf (efrescc,"		 D_ZTCUG5=%.0f\n",icotis->d_ztcug5);
				fprintf (efrescc,"		*part departementale defenitive (D_ZTDPG5) \n");	
				fprintf (efrescc,"		 D_ZTDPG5=%.0f\n",icotis->d_ztdpg5);
				fprintf (efrescc,"		*part communale definitive (D_ZTCNG5) \n");	
				fprintf (efrescc,"		 D_ZTCNG5=%.0f\n",icotis->d_ztcng5);
			}
		}
		else
        {
	  		if (trace)
			{
				fprintf (efrescc,"		*part departementale (D_ZTDPG5) \n");
				fprintf (efrescc,"		 d_ztdpg5=arrondi((%.0f*100)-(%.0f+%0f))\n",icotis->d_zxtpg5,icotis->d_ztcng5,icotis->d_ztcug5);
				fprintf (efrescc,"		 D_ZTDPG5=%.0f\n",icotis->d_ztdpg5);
			}
		}
	}

	icotis->d_zxtpg6 = arrondi(ibase->d_zttpg6*Tcanalchim/1000);
	if (trace)
	{
		fprintf (efrescc,"		*TRANSPORT PRODUITS CHIMIQUES\n");
		fprintf (efrescc,"		 d_zxtpg6=(arrondi(%.0f*%.0f/1000) \n",ibase->d_zttpg6,Tcanalchim);
		fprintf (efrescc,"		 D_ZXTPG6=%.0f\n",icotis->d_zxtpg6);
	}

	if (icotis->d_zxtpg6 > 0)
	{
		if (strncmp(itaux->c_cicuva, "N",sizeof(itaux->c_cicuva)) == 0)
		{
			icotis->d_ztcug6 = 0;
			icotis->d_ztcng6=(icotis->d_zxtpg6*50);
		    if (trace) fprintf (efrescc,"		* commune isolee   \n");
	    }
		if (strncmp(itaux->c_cicuu, "A",sizeof(itaux->c_cicuu)) == 0)
		{
			icotis->d_ztcng6 = ((icotis->d_zxtpg6*itaux->d_zxcna3)/100);
			icotis->d_ztcug6=((icotis->d_zxtpg6*itaux->d_zxcua3)/100);
			if (trace) fprintf (efrescc,"		*commune membre d un EPCI add  \n");	
	    }
		if ((strncmp(itaux->c_cicuu, "U",sizeof(itaux->c_cicuu)) == 0) || (strncmp(iindic->c_cicngp,"O",sizeof(iindic->c_cicngp)) == 0))
		{
			icotis->d_ztcng6 = 0;
			icotis->d_ztcug6 = arrondi (icotis->d_zxtpg6*50);
		    if (trace) fprintf (efrescc,"		*commune membre d un EPCI tpu  \n");	
		}
		if (trace)
		{
			fprintf (efrescc,"		*part communale \n");
			fprintf (efrescc,"		 d_ztcng6=(%.0f*50)\n",icotis->d_zxtpg6);
			fprintf (efrescc,"		 D_ZTCNG6=%.0f\n",icotis->d_ztcng6);
		}

		if ((!grand_lyon) && (trace))
		{
			if (trace)
			{
				fprintf (efrescc,"		*Part epci \n");		
				fprintf (efrescc,"		 D_ZTCUG6=%.0f\n",icotis->d_ztcug6);
			}
		}

		icotis->d_ztdpg6 =  ((icotis->d_zxtpg6*100) - (icotis->d_ztcng6 + icotis->d_ztcug6));
		if (grand_lyon)
		{
			icotis->d_ztcug6 = arrondi (icotis->d_zxtpg6 * 100);
			icotis->d_ztdpg6 = 0;
			icotis->d_ztcng6 = 0;
			if (trace)
			{
				fprintf (efrescc,"		*PART EPCI RECALCULEE (montant IFER(ZXTPG6) * 100) \n");	
				fprintf (efrescc,"		 D_ZXTPG6=%.0f\n",icotis->d_zxtpg6);
				fprintf (efrescc,"		 D_ZTCUG6=%.0f\n",icotis->d_ztcug6);
				fprintf (efrescc,"		*part departementale definitive (D_ZTDPG6) \n");	
				fprintf (efrescc,"		 D_ZTDPG6=%.0f\n",icotis->d_ztdpg6);
				fprintf (efrescc,"		*part communale definitive (D_ZTCNG6) \n");	
				fprintf (efrescc,"		 D_ZTCNG6=%.0f\n",icotis->d_ztcng6);
			}
		}
		else
        {
			if (trace)
			{
				fprintf (efrescc,"		*part departementale (D_ZTDPG6) \n");
				fprintf (efrescc,"		 d_ztdpg6=arrondi((%.0f*100)-(%.0f+%0f))\n",icotis->d_zxtpg6,icotis->d_ztcng6,icotis->d_ztcug6);
				fprintf (efrescc,"		 D_ZTDPG6=%.0f\n",icotis->d_ztdpg6);
			}
		}
	}

	icotis->d_zttpg1=arrondi((icotis->d_zxtpg1+icotis->d_zxtpg2+icotis->d_zxtpg3+icotis->d_zxtpg4+icotis->d_zxtpg5+icotis->d_zxtpg6+icotis->d_zxtpg8)*0.01);
	icotis->d_zttpg2=arrondi((icotis->d_zxtpg1+icotis->d_zxtpg2+icotis->d_zxtpg3+icotis->d_zxtpg4+icotis->d_zxtpg5+icotis->d_zxtpg6+icotis->d_zxtpg8)*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion 1519HA \n");
		fprintf (efrescc,"		*FAR 1519HA 1%% \n");
		fprintf (efrescc,"		 D_ZTTPG1=arrondi(%.0f+%.0f+%.0f)\n",icotis->d_zxtpg1,icotis->d_zxtpg2,icotis->d_zxtpg3);
		fprintf (efrescc,"		 ((+%.0f+%.0f+%.0f+%.0f) x 0.01)\n",icotis->d_zxtpg4,icotis->d_zxtpg5,icotis->d_zxtpg6,icotis->d_zxtpg8);
		fprintf (efrescc,"		 D_ZTTPG1=%.0f\n",icotis->d_zttpg1);
		fprintf (efrescc,"		*FDNV 1519HA 2%% \n");
		fprintf (efrescc,"		 D_ZTTPG2=arrondi(%.0f+%.0f+%.0f)\n",icotis->d_zxtpg1,icotis->d_zxtpg2,icotis->d_zxtpg3);
		fprintf (efrescc,"		 ((+%.0f+%.0f+%.0f+%.0f) x 0.01)\n",icotis->d_zxtpg4,icotis->d_zxtpg5,icotis->d_zxtpg6,icotis->d_zxtpg8);
		fprintf (efrescc,"		 D_ZTTPG2=%.0f\n",icotis->d_zttpg2);
	}

	if (trace)
	{
		fprintf (efrescc,"		***  1519 HB**** \n");	
		fprintf (efrescc,"		*GEOTHERMIE (D-ZTTPOE) \n");	
		fprintf (efrescc,"		* puissance \n");	
		fprintf (efrescc,"		 D_ZTETGE=%.0f\n",ibase->d_ztetge);
	}
	if ((ibase->d_ztetge < 12000) && (iindic->s_zttprs == 0))
	{
		icotis->d_zttpoe = 0;
	}
	if ((ibase->d_ztetge >= 12000) || (iindic->s_zttprs == 1))
	{
	    icotis->d_zttpoe=arrondi(ibase->d_ztetge*Tgeothermie);
	}
	if (trace)
	{
		fprintf (efrescc,"		 d_zttpoe=arrondi(%.0f*%.0f) \n",ibase->d_ztetge,Tgeothermie);
		fprintf (efrescc,"		 D_ZTTPOE=%.0f\n",icotis->d_zttpoe);
	}

	if (icotis->d_zttpoe > 0)
	{
		icotis->d_ztcnoe = arrondi(icotis->d_zttpoe*60);
		icotis->d_ztrgoe = arrondi((icotis->d_zttpoe*100) - icotis->d_ztcnoe);
		if (trace)
		{
			fprintf (efrescc,"		*part communale (D_ZTCNOE) \n");	
			fprintf (efrescc,"		 D_ZTCNOE=%.0f\n",icotis->d_ztcnoe);
			fprintf (efrescc,"		*part régionale D_ZTRGOE\n");	
			fprintf (efrescc,"		 D_ZTRGOE%.0f\n",icotis->d_ztrgoe);
		}
	}

	icotis->d_zttpo1=arrondi(icotis->d_zttpoe*0.01);
	icotis->d_zttpo2=arrondi(icotis->d_zttpoe*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion 1519HB \n");	
		fprintf (efrescc,"		*FAR 1519HB 1%% \n");
		fprintf (efrescc,"		 D_ZTTPO1=arrondi(%.0f x 0.01)\n",icotis->d_zttpoe);
		fprintf (efrescc,"		 D_ZTTPO1=%.0f\n",icotis->d_zttpo1);
		fprintf (efrescc,"		*FDNV 1519HB 2%% \n");
		fprintf (efrescc,"		 D_ZTTPO2=arrondi(%.0f x 0.02)\n",icotis->d_zttpoe);
		fprintf (efrescc,"		 D_ZTTPO2=%.0f\n",icotis->d_zttpo2);
	}

	icotis->d_zttpr1=arrondi(ibase->d_ztsn01*Tlocodies);
	if (trace)
	{
		fprintf (efrescc,"		*Automoteur à moteur thermique(D-ZTTPR1) \n");	
		fprintf (efrescc,"		 d_zttpr1=%.0f*%.0f \n",ibase->d_ztsn01,Tlocodies);
		fprintf (efrescc,"		 D_ZTTPR1=%.0f\n",icotis->d_zttpr1);
	}
	icotis->d_zttpr2=arrondi(ibase->d_ztsn02*Tlocodies);
	if (trace)
	{
		fprintf (efrescc,"		*Locomotive diesel(D-ZTTPR2) \n");	
		fprintf (efrescc,"		 d_zttpr2=%.0f*%.0f \n",ibase->d_ztsn02,Tlocodies);
		fprintf (efrescc,"		 D_ZTTPR2=%.0f\n",icotis->d_zttpr2);
	}
	icotis->d_zttpr3=arrondi(ibase->d_ztsn03*Tmotrice);
	if (trace)
	{
		fprintf (efrescc,"		*Automoteur à moteur electrique(D-ZTTPR3) \n");	
		fprintf (efrescc,"		 d_zttpr3=%.0f*%.0f \n",ibase->d_ztsn03,Tmotrice);
		fprintf (efrescc,"		 D_ZTTPR3=%.0f\n",icotis->d_zttpr3);
	}
	icotis->d_zttpr4=arrondi(ibase->d_ztsn04*Tlocoelec);
	if (trace)
	{
		fprintf (efrescc,"		*Locomotive electrique(D-ZTTPR4) \n");	
		fprintf (efrescc,"		 d_zttpr4=%.0f*%.0f \n",ibase->d_ztsn04,Tlocoelec);
		fprintf (efrescc,"		 D_ZTTPR4=%.0f\n",icotis->d_zttpr4);
	}
	icotis->d_zttpr5=arrondi(ibase->d_ztsn05*Tmottgv);
	if (trace)
	{
		fprintf (efrescc,"		*Motrice TGV (D-ZTTPR5) \n");	
		fprintf (efrescc,"		 d_zttpr5=%.0f*%.0f \n",ibase->d_ztsn05,Tmottgv);
		fprintf (efrescc,"		 D_ZTTPR5=%.0f\n",icotis->d_zttpr5);
	}
	icotis->d_zttpr8=arrondi(ibase->d_ztsn08*Tautomot);
	if (trace)
	{
		fprintf (efrescc,"		*Motrice automotrice tram-train (D-ZTTPR8) \n");	
		fprintf (efrescc,"		 d_zttpr8=%.0f*%.0f \n",ibase->d_ztsn08,Tautomot);
		fprintf (efrescc,"		 D_ZTTPR8=%.0f\n",icotis->d_zttpr8);
	}
	icotis->d_zttpr6=arrondi(ibase->d_ztsn06*Twagon);
	if (trace)
	{
		fprintf (efrescc,"		*Remorque transport voyageur (D-ZTTPR6) \n");	
		fprintf (efrescc,"		 d_zttpr6=%.0f*%.0f \n",ibase->d_ztsn06,Twagon);
		fprintf (efrescc,"		 D_ZTTPR6=%.0f\n",icotis->d_zttpr6);
	}
	icotis->d_zttpr7=arrondi(ibase->d_ztsn07*Twagontgv);
	if (trace)
	{
		fprintf (efrescc,"		*Remorque transport voyageur TGV (D-ZTTPR7) \n");	
		fprintf (efrescc,"		 d_zttpr7=%.0f*%.0f \n",ibase->d_ztsn07,Twagontgv);
		fprintf (efrescc,"		 D_ZTTPR7=%.0f\n",icotis->d_zttpr7);
	}
	icotis->d_zttpr9=arrondi(ibase->d_ztsn09*Twagontt);
	if (trace)
	{
		fprintf (efrescc,"		*Remorque tram-train (D-ZTTPR9) \n");	
		fprintf (efrescc,"		 d_zttpr9=%.0f*%.0f \n",ibase->d_ztsn09,Twagontt);
		fprintf (efrescc,"		 D_ZTTPR9=%.0f\n",icotis->d_zttpr9);
	}
	if (ibase->d_ztsn10 < 300000)
	{
		icotis->d_zztpmr = 0;
	}
	else
	{
        if (ibase->d_ztsn10 < 1700000)
		{
			icotis->d_zztpmr = (((ibase->d_ztsn10 - 300000) / 1400000) * 10000000);
		}
		else
		{
	        icotis->d_zztpmr = 10000000;
		}
	}
   	if (trace)
	{
		fprintf (efrescc,"		*** COEFFICIENT REDUCTEUR **** \n");	
        fprintf (efrescc,"		*Nb de KM parcouru en N-1 (D-ZTSN10) \n");
	    fprintf (efrescc,"		 D_ZTSN10=%.0f\n",ibase->d_ztsn10);
		fprintf (efrescc,"		*Coefficient mat. roulant (calc)  (D-ZZTPMR) \n");
		fprintf (efrescc,"		 D_ZZTPMR=%.0f\n",icotis->d_zztpmr);
	}
	
	zttpro=(icotis->d_zttpr1+icotis->d_zttpr2+icotis->d_zttpr3+icotis->d_zttpr4+icotis->d_zttpr5+icotis->d_zttpr6+icotis->d_zttpr7
					+icotis->d_zttpr8+icotis->d_zttpr9) ;
	if (trace)
	{
		fprintf (efrescc,"		*Cumul montant IFER materiel roulant (ZTTPRO) \n");	
		fprintf (efrescc,"		 zttpro=%.0f+%.0f+%.0f\n",icotis->d_zttpr1,icotis->d_zttpr2,icotis->d_zttpr3);
		fprintf (efrescc,"		 +%.0f+%.0f+%.0f+%.0f \n",icotis->d_zttpr4,icotis->d_zttpr5,icotis->d_zttpr6,icotis->d_zttpr7);
		fprintf (efrescc,"		 +%.0f+%.0f\n",icotis->d_zttpr8,icotis->d_zttpr9);
		fprintf (efrescc,"		 zttpro=%.0f\n",zttpro);
	}

	icotis->d_zttpmr = arrondi ((zttpro * icotis->d_zztpmr) / 10000000);
    if (trace)
	{
		fprintf (efrescc,"		***  Cotisation nette materiel roulant avant frais gestion**** \n");	
		fprintf (efrescc,"		 d_zttpmr=arrondi(%.0f*%.0f) \n",zttpro,icotis->d_zztpmr);
		fprintf (efrescc,"		 D_ZTTPMR=%.0f\n",icotis->d_zttpmr);
	}
	
	icotis->d_zttpq1=arrondi(icotis->d_zttpmr*0.01);
	icotis->d_zttpq2=arrondi(icotis->d_zttpmr*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion 1599 QUATER A  \n");	
		fprintf (efrescc,"		*FAR 1599 QUATER A  1%% \n");
		fprintf (efrescc,"		 D_ZTTPQ1=arrondi(%.0f  x 0.01)\n",icotis->d_zttpmr);
		fprintf (efrescc,"		*FDNV 1599 QUATER A  2%% \n");
		fprintf (efrescc,"		 D_ZTTPQ2=arrondi(%.0f  x 0.01)\n",icotis->d_zttpmr);
	}

	icotis->d_zzsnr1=arrondi(ibase->d_ztsn11*Tmetro);
	if (trace)
	{
		fprintf (efrescc,"		***  1519 QUATER A BIS **** \n");
		fprintf (efrescc,"		*Motrice metro (D-ZZSNR1) \n");	
		fprintf (efrescc,"		 d_zzsnr1=(arrondi(%.0f*%.0f) \n",ibase->d_ztsn11,Tmetro);
		fprintf (efrescc,"		 D_ZZSNR1=%.0f\n",icotis->d_zzsnr1);
	}
	icotis->d_zzsnr2=arrondi(ibase->d_ztsn12*Tmetro);
	if (trace)
	{
		fprintf (efrescc,"		*Remorque metro (D-ZZSNR2) \n");	
		fprintf (efrescc,"		 d_zzsnr2=(arrondi(%.0f*%.0f) \n",ibase->d_ztsn12,Tmetro);
		fprintf (efrescc,"		 D_ZZSNR2=%.0f\n",icotis->d_zzsnr2);
	}
	icotis->d_zzsnr3=arrondi(ibase->d_ztsn13*Tautometro);
	if (trace)
	{
		fprintf (efrescc,"		*Autres motrices (D-ZZSNR3) \n");	
		fprintf (efrescc,"		 d_zzsnr3=(arrondi(%.0f*%.0f) \n",ibase->d_ztsn13,Tautometro);
		fprintf (efrescc,"		 D_ZZSNR3=%.0f\n",icotis->d_zzsnr3);
	}
	icotis->d_zzsnr4=arrondi(ibase->d_ztsn14*Twagon);
	if (trace)
	{
		fprintf (efrescc,"		*autres remorques(D-ZZSNR4) \n");	
		fprintf (efrescc,"		 d_zzsnr4=(arrondi(%.0f*%.0f) \n",ibase->d_ztsn14,Twagon);
		fprintf (efrescc,"		 D_ZZSNR4=%.0f\n",icotis->d_zzsnr4);
	}
	icotis->d_ztsnf1=arrondi((icotis->d_zzsnr1+icotis->d_zzsnr2+icotis->d_zzsnr3+icotis->d_zzsnr4)*0.01);
	icotis->d_ztsnf2=arrondi((icotis->d_zzsnr1+icotis->d_zzsnr2+icotis->d_zzsnr3+icotis->d_zzsnr4)*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion QUATER A BIS \n");
		fprintf (efrescc,"		*FAR QUATER A BIS 1%% \n");
		fprintf (efrescc,"		 D_ZTSNF1=arrondi(%.0f+%.0f+%.0f)\n",icotis->d_zzsnr1,icotis->d_zzsnr2,icotis->d_zzsnr3);
		fprintf (efrescc,"		 (f+%.0f x 0.01)\n",icotis->d_zzsnr4);
		fprintf (efrescc,"		 D_ZTSNF1=%.0f\n",icotis->d_ztsnf1);
		fprintf (efrescc,"		*FDNV QUATER A BIS 2%% \n");
		fprintf (efrescc,"		 D_ZTSNF2=arrondi(%.0f+%.0f+%.0f)\n",icotis->d_zzsnr1,icotis->d_zzsnr2,icotis->d_zzsnr3);
		fprintf (efrescc,"		 (f+%.0f x 0.02)\n",icotis->d_zzsnr4);
		fprintf (efrescc,"		 D_ZTSNF2=%.0f\n",icotis->d_ztsnf2);
	}

	icotis->d_zxtpr2=arrondi(ibase->d_zztpr2*Trepartiteur);
	if (trace)
	{
		fprintf (efrescc,"		*Ligne boucle de cuivre(D-ZXTPR2) \n");	
		fprintf (efrescc,"		 d_zxtpr2=(arrondi(%.0f*%.2f) \n",ibase->d_zztpr2,Trepartiteur);
		fprintf (efrescc,"		 D_ZXTPR2=%.0f\n",icotis->d_zxtpr2);
	}
	icotis->d_zxtpr4=arrondi(ibase->d_zztpr4*Trepartiteur);
	if (trace)
	{
		fprintf (efrescc,"		*Ligne point de mutualisation(D-ZXTPR4) \n");	
		fprintf (efrescc,"		 d_zxtpr4=(arrondi(%.0f*%.2f) \n",ibase->d_zztpr4,Trepartiteur);
		fprintf (efrescc,"		 D_ZXTPR4=%.0f\n",icotis->d_zxtpr4);
	}
	icotis->d_zxtpr6=arrondi(ibase->d_zztpr6*Trepartiteur);
	if (trace)
	{
		fprintf (efrescc,"		*Ligne moeuds de raccordement(D-ZXTPR6) \n");	
		fprintf (efrescc,"		 d_zxtpr6=(arrondi(%.0f*%.2f) \n",ibase->d_zztpr6,Trepartiteur);
		fprintf (efrescc,"		 D_ZXTPR6=%.0f\n",icotis->d_zxtpr6);
	}
	icotis->d_zytpr1=arrondi((icotis->d_zxtpr2 + icotis->d_zxtpr4 + icotis->d_zxtpr6)*0.01);
	icotis->d_zytpr2=arrondi((icotis->d_zxtpr2 + icotis->d_zxtpr4 + icotis->d_zxtpr6)*0.02);
	if (trace)
	{
		fprintf (efrescc,"		*Frais de gestion QUATER B \n");
		fprintf (efrescc,"		*FAR 1599 QUATER B 1%% \n");
		fprintf (efrescc,"		 D_ZYTPR1=arrondi((%.0f+%.0f+%.0f) X 0.01)\n",icotis->d_zxtpr2,icotis->d_zxtpr4,icotis->d_zxtpr6);
		fprintf (efrescc,"		 D_ZYTPR1=%.0f\n",icotis->d_zytpr1);
		fprintf (efrescc,"		*FDNV 1599 QUATER B 2%% \n");
		fprintf (efrescc,"		 D_ZYTPR2=arrondi((%.0f+%.0f+%.0f) X 0.02)\n",icotis->d_zxtpr2,icotis->d_zxtpr4,icotis->d_zxtpr6);
		fprintf (efrescc,"		 D_ZYTPR2=%.0f\n",icotis->d_zytpr2);
	}

if (trace)
{
	fprintf (efrescc," [- IFERCALC2021.C fin -]\n");
	fprintf (efrescc," *******************************\n");
}
return 0;
}




